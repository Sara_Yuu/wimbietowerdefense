using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 10;
    public Transform target;
    public ParticleSystem partSyst;
    public float delayTime = 2f;
    private GameObject towers;


    void FixedUpdate()
    {
        if (target)
        {
            // Fly towards the target
            Vector3 dir = target.position - transform.position;
            GetComponent<Rigidbody>().velocity = dir.normalized * speed;
            StartCoroutine(SlowAnimation());
            partSyst.Play();
        }
        else
        {
            // Otherwise destroy self
            Destroy(gameObject);
        }
    }
    void OnTriggerEnter(Collider col)
    {
        Health health = col.GetComponentInChildren<Health>();
        if (health)
        {
            health.DecreasingHealth();
            Destroy(gameObject);
        }
    }
    
    IEnumerator SlowAnimation()
    {
        yield return new WaitForSeconds(delayTime);
        
    }
}
