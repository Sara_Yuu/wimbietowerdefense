using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buildPlace : MonoBehaviour
{
    private GameObject towers;
    public Vector3 positionOffset; 
    public float buildRange = 6f;
    public string towerTag = "Wizard";
    float closestDistance = Mathf.Infinity;
    GameObject nearestTower = null;

    public GameObject bombPrefab;
    public float explosionPower = 8f;
    public float explosionRadius = 5f;
    public float upForce = 1f;
    Rigidbody rb;
    private Transform objectToFind;
    bool isSpawned = false;
    public float delayTime = 2f;

     void OnMouseUpAsButton() 
     {
         objectToFind = transform.childCount > 0 ? transform.GetChild(0) : null;
        if(objectToFind != null && objectToFind.tag == "Bomb")
        {
            isSpawned = true;
            Explode();
            Destroy(gameObject);
        }
        else
        {
            TryBuildTower();
        }
        // if (!TryBuildTower()){
        //     Debug.Log("Can't build");
        // }
    }

    void Explode()
    {
        Vector3 explodePosition = bombPrefab.transform.position;
        Collider[] colliders = Physics.OverlapSphere(explodePosition , explosionRadius);
        foreach(Collider hit in colliders)
        {
            rb = hit.GetComponent<Rigidbody>();
            if(rb != null)
            {
                rb.AddExplosionForce(explosionPower , explodePosition, explosionRadius , upForce, ForceMode.Impulse);
            }
        }
    }


    bool TryBuildTower()
    {
        
        if((towers != null && closestDistance < buildRange) || !CanBuildTower()) return false;
        GameObject wizardsToAdd = BuildManager.instance.GetWizardsToAdd();
        towers = (GameObject)Instantiate(wizardsToAdd);
        towers.transform.position = transform.position + positionOffset + Vector3.up;
        return true;
    }

    bool CanBuildTower()
    {
        GameObject[] towers = GameObject.FindGameObjectsWithTag(towerTag);
        foreach(GameObject tower in towers)
        {
            float distanceToTower = Vector3.Distance(transform.position , tower.transform.position);
            if(distanceToTower > closestDistance)
            {
                closestDistance = distanceToTower;
                nearestTower = tower;
            }
        }
        if(closestDistance >= buildRange)
        {
            Debug.Log("Build Tower");
            return true;
        }
        else
        {
            Debug.Log("Don't build tower");
            return false;
        }
    }
}
