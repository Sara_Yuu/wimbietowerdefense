using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    Animator animator;
    void Start()
    {
        animator = GetComponent<Animator>();
        GameObject castle = GameObject.Find("Castle");
        if (castle)
            GetComponent<NavMeshAgent>().destination = castle.transform.position;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.name == "Castle")
        {
           animator.SetBool("isRunning",false);
           animator.SetBool("isAttacking",true); 
            col.GetComponentInChildren<Health>().DecreasingHealth();
            Destroy(gameObject);
           BuildManager.instance.EndGame();
        }
    }
}
