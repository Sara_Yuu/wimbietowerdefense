using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour
{
        public static BuildManager instance;
        private GameObject wizardsToAdd;
        public GameObject wizardPrefab;
        public GameObject gameOverPanel;

    private void Awake() 
       {
            if(instance == null)
            {
                instance = this;
            }
       }

     void Start()
    {
        wizardsToAdd = wizardPrefab;
        
    }
       public GameObject GetWizardsToAdd()
    {
        return wizardsToAdd;
    }

    public void EndGame()
    {
        gameOverPanel.gameObject.SetActive(true);

    }

}

