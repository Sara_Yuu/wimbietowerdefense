using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RTSCameraController : MonoBehaviour
{
    public float moveSpeed;
    public float zoomSpeed;
    public float minZoomDist;
    public float maxZoomDist;

    public Vector2 moveLimit;
    private Camera cam;
   
    void Awake ()
    {
        cam = Camera.main;
    }

    void Update()
    {
        Move();
        Zoom();
    }
    
    void Move ()
    {
        float xInput = Input.GetAxis("Horizontal");
        float zInput = Input.GetAxis("Vertical");
        Vector3 dir = transform.forward * zInput + transform.right * xInput;
        transform.position += dir * moveSpeed * Time.deltaTime;
        // transform.position = new Vector2(Mathf.Clamp(transform.position.x , -moveLimit.x , moveLimit.x), Mathf.Clamp(transform.position.z , -moveLimit.y , moveLimit.y));
        // xInput = Mathf.Clamp(xInput , -moveLimit.x , moveLimit.x);
        // zInput = Mathf.Clamp(zInput , -moveLimit.y , moveLimit.y);
    }
    void Zoom ()
    {
        float scrollInput = Input.GetAxis("Mouse ScrollWheel");
        float dist = Vector3.Distance(transform.position, cam.transform.position);
        if(dist < minZoomDist && scrollInput > 0.0f)
            return;
        else if(dist > maxZoomDist && scrollInput < 0.0f)
            return;
        cam.transform.position += cam.transform.forward * scrollInput * zoomSpeed;
    }
}
