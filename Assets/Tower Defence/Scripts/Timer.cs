using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Timer : MonoBehaviour
{
    public int secondsLeft = 30;
    public bool takeAway = false;
    public TextMeshProUGUI timerText;

     void Start() 
    {
        timerText = GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        if(takeAway == false && secondsLeft > 0)
        {
            StartCoroutine(TimerTake());
        }
    }
    IEnumerator TimerTake()
    {
        takeAway = true;
        yield return new WaitForSeconds(1);
        secondsLeft -=1;
        if(secondsLeft < 10)
        {
           timerText.text = "00:0" + secondsLeft;
        }
        else
        {
           timerText.text = "00:" + secondsLeft;
        }
        takeAway = false;
    }
}
