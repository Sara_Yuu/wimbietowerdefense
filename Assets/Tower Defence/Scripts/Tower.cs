using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    Animator anim;
    public GameObject bulletPrefab;
    Animator animator;
    public Transform target;
    public Transform wizardRotate;
    public float attackRange = 15f;
   public float buildRange = 6f;
    public string enemyTag = "Enemy";
   public string towerTag = "Wizard";
    // Rotation Speed
   // public float rotationSpeed = 35;
    public void Start() 
    {
        anim = GetComponent<Animator>();
        InvokeRepeating("UpdateTarget",0f , 0.5f);
    }
    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach(GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position , enemy.transform.position);
            if(distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }
        if(nearestEnemy !=null && shortestDistance <= attackRange)
        {
            target = nearestEnemy.transform;
        }
        else
        {
            target = null;
        }
    }

    void Update()
    {
        if(target == null)
        {
            return;
        }
        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = lookRotation.eulerAngles;
        wizardRotate.rotation = Quaternion.Euler(0f , rotation.y , 0f);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.GetComponent<Enemy>())
        {
            anim.SetBool("isAttacking" , true);
            GameObject enemy = (GameObject)Instantiate(bulletPrefab, transform.position, Quaternion.identity);
            enemy.GetComponent<Bullet>().target = col.transform;
        }
    }
    private void OnDrawGizmosSelected() 
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position , attackRange);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position , buildRange);
    }
}
